/**
 * \file
 *         A TCP socket echo server. Listens and replies on port 8080
 * \author
 *         mds
 */

#include "contiki-net.h"
#include "sys/cc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "contiki.h"
#include "buzzer.h"
#include "ieee-addr.h"
#include "random.h"
#include "board-peripherals.h"
#include "ti-lib.h"

#include "dev/watchdog.h"
#include "dev/cc26xx-uart.h"
#include "dev/serial-line.h"
#include "dev/leds.h"

#include "sys/etimer.h"
#include "sys/timer.h"
#include "sys/ctimer.h"

#define SERVER_PORT 80

static struct tcp_socket socket;

#define INPUTBUFSIZE 400
static uint8_t inputbuf[INPUTBUFSIZE];

#define OUTPUTBUFSIZE 400
static uint8_t outputbuf[OUTPUTBUFSIZE];

PROCESS(tcp_server_process, "TCP echo process");
PROCESS(process2, "Timing Action");
PROCESS(process3, "Sensors Action");
AUTOSTART_PROCESSES(&tcp_server_process, &process2, &process3);
static uint8_t get_received;
static int bytes_to_send;


//counters
static int counter_etimer;
static int counter_etimer2;
static int counter_timer;

//Timers
static struct timer timer_timer;		//Clock Tick Timer

unsigned char addr[256];
unsigned char comp_red[]="r";
unsigned char comp_green[]="g";
unsigned char comp_all[]="a";
unsigned char buzz_sw[]="b";
unsigned char buzz_inc[]="i";
unsigned char buzz_dec[]="d";
unsigned char tagip[]="n";
unsigned char sens_pressure[]="p";
unsigned char sens_humidity[]="h";

bool led_red = false;
bool led_green = false;
bool led_all = false;
bool buzzer = false;
bool buzz_long = false;
bool buzz_incr=false;
unsigned int buzz_freq = 1000;
uint8_t macAddress[8];
char myMessage[512];

short activePressure = -1;
short activeHumidity = -1;

int i;
int humidity_pres_val;
int humidity_val;
int sample_freq=10;

//Intialise Humidity sensor
static void humidity_init(void *not_used) {
	SENSORS_ACTIVATE(hdc_1000_sensor);
	SENSORS_ACTIVATE(bmp_280_sensor);
}

void do_Actions(){
	counter_etimer++;
	if(timer_expired(&timer_timer)) {
    	counter_timer++;
	}
	if (led_all){
		leds_toggle(LEDS_ALL);
		clock_wait(CLOCK_SECOND);
		leds_off(LEDS_ALL);
	} else if (led_green){
		leds_toggle(LEDS_GREEN);
		clock_wait(CLOCK_SECOND);
		leds_off(LEDS_GREEN);
	} else if (led_red){
		leds_toggle(LEDS_RED);
		clock_wait(CLOCK_SECOND);
		leds_off(LEDS_RED);
	} else {
		leds_off(LEDS_ALL);
	}
}

void do_Sensors(){
	counter_etimer2++;
	counter_timer++;
	
	if(activeHumidity>=0 && activeHumidity <= sample_freq){
		humidity_val = hdc_1000_sensor.value(HDC_1000_SENSOR_TYPE_HUMIDITY);//Read Humidity value
		printf("Humidity=%d.%02d %%RH\n\r", humidity_val/100, humidity_val%100);//Display values
		sprintf(myMessage,"Humidity=%d.%02d %%RH\n\r", humidity_val/100, humidity_val%100);
		tcp_socket_send_str(&socket, myMessage);
		clock_wait(CLOCK_SECOND*0.5);
		activeHumidity++;
	}
	if(activePressure>=0 && activePressure <= sample_freq){
		humidity_pres_val = hdc_1000_sensor.value(BMP_280_SENSOR_TYPE_PRESS);
		printf("Pressure:%d.%02d Pa\n\r", humidity_pres_val/100, humidity_pres_val%100);
		sprintf(myMessage,"Pressure:%d.%02d Pa\n\r", humidity_pres_val/100, humidity_pres_val%100);
		tcp_socket_send_str(&socket, myMessage);
		clock_wait(CLOCK_SECOND*0.5);
		activePressure++;
	}
	if(activeHumidity>=sample_freq){activeHumidity = -1;tcp_socket_close(&socket);}
	if(activePressure>=sample_freq){activePressure = -1;tcp_socket_close(&socket);}
}
/*---------------------------------------------------------------------------*/
//Input data handler
static int input(struct tcp_socket *s, void *ptr, const uint8_t *inputptr, int inputdatalen) {

	printf("input %d bytes '%s'\n\r", inputdatalen, inputptr);
	//tcp_socket_send_str(&socket, inputptr);	//Reflect byte
	char inputchar = inputptr[0];
	printf("Inputs:%c\t%c\n\r",inputptr[5],inputchar);
	SENSORS_ACTIVATE(hdc_1000_sensor);
	SENSORS_ACTIVATE(bmp_280_sensor);
	
	if(inputptr[5]=='l'&&inputptr[6]=='e'&&inputptr[7]=='d'&&inputptr[8]=='s'){
		if(inputptr[10]=='r'){
			tcp_socket_send_str(&socket,"Switching red LED\n\r");
			printf("Switching red LED \n\r");
			if (led_all){led_all=false;led_green=true;led_red=false;}
			else if(led_green){led_all=true;led_green=false;led_red=false;}
			else if (led_red){led_red = false;}
			else {led_red=true;}
		}
		if(inputptr[10]=='g'){
			tcp_socket_send_str(&socket,"Switching green LED\n\r");
			printf("Switching green LED \n\r");
			if (led_all){led_all=false;led_green=false;led_red=true;}
			else if (led_red){led_all=true;led_green=false;led_red=false;}
			else if (led_green){ led_green = false;}
			else {led_green = true;}
		}
		if(inputptr[10]=='a'){
			tcp_socket_send_str(&socket,"Switching all LEDs \n\r");
			printf("Switching all LEDs \n\r");
			if (led_all){led_all=false;led_green=false;led_red=false;}	
			else {led_all=true;led_green=false;led_red=false;}
		}
		tcp_socket_close(&socket);
	}
	if(inputptr[5]=='b'&&inputptr[6]=='u'&&inputptr[7]=='z'&&inputptr[8]=='z'&&inputptr[9]=='e'&&inputptr[10]=='r'){
		if((int)inputptr[13]>57 || (int)inputptr[13]<48){
			buzz_freq = (int)inputptr[12]-48;
		} else if((int)inputptr[14]>57 || (int)inputptr[14]<48){
			buzz_freq = ((int)inputptr[12]-48)*10+ (int)inputptr[13]-48;
		} else if((int)inputptr[15]>57 || (int)inputptr[15]<48){
			buzz_freq = (int)inputptr[14]-48+((int)inputptr[13]-48)*10+((int)inputptr[12]-48)*100;
		} else if((int)inputptr[16]>57 || (int)inputptr[16]<48){
			buzz_freq = (int)inputptr[15]-48+((int)inputptr[14]-48)*10+((int)inputptr[13]-48)*100+((int)inputptr[12]-48)*1000;
		}
		printf("Switching buzzer\n\r");
		tcp_socket_send_str(&socket, "Switching buzzer\n\r");
		if (buzzer){buzzer = false;} else {buzzer=true;}
		tcp_socket_close(&socket);
	}
//	if(inputptr[5]=='i'){
//		printf("Increasing buzzer frequency\n\r");
//		tcp_socket_send_str(&socket, "Increasing buzzer frequency\n\r");
//		buzz_long=true;buzz_incr=true;
//	}
//	if(inputptr[5]=='d'){
//		printf("Decreasing buzzer frequency\n\r");
//		tcp_socket_send_str(&socket, "Decreasing buzzer frequency\n\r");
//		buzz_long=true;buzz_incr=false;
//	}
//	if(inputptr[5]=='n'){
//		ieee_addr_cpy_to(macAddress,8);
//		printf("MAC Address is: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n\r",macAddress[0],macAddress[1],macAddress[2],macAddress[3],macAddress[4],macAddress[5],macAddress[6],macAddress[7]);
		
//		sprintf(myMessage,"MAC Address is: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n\r",macAddress[0],macAddress[1],macAddress[2],macAddress[3],macAddress[4],macAddress[5],macAddress[6],macAddress[7]);
//		tcp_socket_send_str(&socket, myMessage);
//	}
	if(inputptr[5]=='p'&&inputptr[6]=='r'&&inputptr[7]=='e'&&inputptr[8]=='s'&&inputptr[9]=='s'&&inputptr[10]=='u'&&inputptr[11]=='r'&&inputptr[12]=='e'){
		if((int)inputptr[15]>57 || (int)inputptr[15]<48){
			sample_freq = (int)inputptr[14]-48;
		} else if((int)inputptr[16]>57 || (int)inputptr[16]<48){
			sample_freq = ((int)inputptr[14]-48)*10+ (int)inputptr[15]-48;
		}
		printf("Active pressure sensor\n\r");
		//tcp_socket_send_str(&socket, "Active pressure sensor\n\r");
		activePressure=0;
	}
	if(inputptr[5]=='h'&&inputptr[6]=='u'&&inputptr[7]=='m'&&inputptr[8]=='i'&&inputptr[9]=='d'&&inputptr[10]=='i'&&inputptr[11]=='t'&&inputptr[12]=='y'){
		if((int)inputptr[15]>57 || (int)inputptr[15]<48){
			sample_freq = (int)inputptr[14]-48;
		} else if((int)inputptr[16]>57 || (int)inputptr[16]<48){
			sample_freq = ((int)inputptr[14]-48)*10+ (int)inputptr[15]-48;
		}
		printf("Active humidity sensor\n\r");
		//tcp_socket_send_str(&socket, "Active humidity sensor\n\r");
		activeHumidity=0;
	}
	//tcp_socket_close(&socket);
	//Clear buffer
	memset(inputptr, 0, inputdatalen);
    return 0; // all data consumed 
}

/*---------------------------------------------------------------------------*/
//Event handler
static void event(struct tcp_socket *s, void *ptr, tcp_socket_event_t ev) {
	printf("event %d\n", ev);
}

/*---------------------------------------------------------------------------*/
//TCP Server process
PROCESS_THREAD(tcp_server_process, ev, data) {
	static struct etimer timer_etimer;
  	PROCESS_BEGIN();

	//Register TCP socket
  	tcp_socket_register(&socket, NULL,inputbuf, sizeof(inputbuf),outputbuf, sizeof(outputbuf),input, event);
  	tcp_socket_listen(&socket, SERVER_PORT);

	printf("Listening on %d\n", SERVER_PORT);
	
	while(1) {
		if(buzzer && buzz_long){
			counter_etimer++;			
			for (i=0;i<5;i++){
				if(buzz_incr){buzz_freq+=10;}
				else {buzz_freq-=10;}
				buzzer_start(buzz_freq);
				etimer_set(&timer_etimer,1 * CLOCK_SECOND);		
				PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
				buzzer_stop();										
			}
			buzz_long=false;
		} else if(buzzer){
			counter_etimer++;
			buzzer_start(buzz_freq);
			etimer_set(&timer_etimer,0.2 * CLOCK_SECOND);		
			PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
			buzzer_stop();
	}
		//Wait for event to occur
		PROCESS_PAUSE();
	}
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
//LEDs Action thread
PROCESS_THREAD(process2, ev, data) {
	static struct etimer timer_etimer;
	PROCESS_BEGIN();

  while(1) {
	timer_set(&timer_timer, 1 * CLOCK_SECOND);
	etimer_set(&timer_etimer, CLOCK_SECOND);
	PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
	do_Actions();
	//PROCESS_YIELD();
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
//Sensor Action thread
PROCESS_THREAD(process3, ev, data) {
	static struct etimer timer_etimer;
	PROCESS_BEGIN();
  while(1) {
	timer_set(&timer_timer, 0.5 * CLOCK_SECOND);
	etimer_set(&timer_etimer, CLOCK_SECOND);
	PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
	do_Sensors();
	SENSORS_ACTIVATE(hdc_1000_sensor);
	SENSORS_ACTIVATE(bmp_280_sensor);
	PROCESS_YIELD();
  }

  PROCESS_END();
}

