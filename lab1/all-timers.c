/*
 * Copyright (C) 2015, Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "contiki.h"
#include "dev/cc26xx-uart.h"
#include "dev/serial-line.h"
#include "dev/leds.h"
#include "buzzer.h"
#include "ieee-addr.h"
#include "sys/etimer.h"
#include "sys/timer.h"


PROCESS(process1, "Main Process");
PROCESS(process2, "Timing Action");
//PROCESS(process3, "Buzzing Action");
AUTOSTART_PROCESSES(&process1, &process2);

//counters
static int counter_etimer;
static int counter_etimer2;
static int counter_timer;

//Timers
static struct timer timer_timer;		//Clock Tick Timer

unsigned char addr[256];
unsigned char comp_red[]="r";
unsigned char comp_green[]="g";
unsigned char comp_all[]="a";
unsigned char buzz_sw[]="b";
unsigned char buzz_inc[]="i";
unsigned char buzz_dec[]="d";
unsigned char tagip[]="n";

bool led_red = false;
bool led_green = false;
bool led_all = false;
bool buzzer = false;
bool buzz_long = false;
bool buzz_incr=false;
unsigned int buzz_freq = 1000;
uint8_t macAddress[8];

int i;

// Timing Actions response
void do_Actions(){
	counter_etimer++;
	if(timer_expired(&timer_timer)) {
    	counter_timer++;
	}
	
	if (led_all){
		leds_toggle(LEDS_ALL);
		clock_wait(CLOCK_SECOND);
		leds_off(LEDS_ALL);
	} else if (led_green){
		leds_toggle(LEDS_GREEN);
		clock_wait(CLOCK_SECOND);
		leds_off(LEDS_GREEN);
	} else if (led_red){
		leds_toggle(LEDS_RED);
		clock_wait(CLOCK_SECOND);
		leds_off(LEDS_RED);
	} else {
		leds_off(LEDS_ALL);
	}
}

/*---------------------------------------------------------------------------*/
//Event timer thread
PROCESS_THREAD(process1, ev, data) {
	static struct etimer timer_etimer;
  PROCESS_BEGIN();
	cc26xx_uart_set_input(serial_line_input_byte);
  while(1) {
	PROCESS_YIELD();	//Let other threads run
	if(ev == serial_line_event_message) {
		printf("received line: %s\n\r", (char *)data);
//		printf("LED return:%d\n\r",leds_get());
		if (strcmp((char*)data,(char*)comp_red) == 0){
			printf("Switching red LED \n\r");
			if (led_all){led_all=false;led_green=true;led_red=false;}
			else if(led_green){led_all=true;led_green=false;led_red=false;}
			else if (led_red){led_red = false;}
			else {led_red=true;}
		}
		if (strcmp((char*)data,(char*)comp_green) == 0){
			printf("Switching green LED \n\r");
			if (led_all){led_all=false;led_green=false;led_red=true;}
			else if (led_red){led_all=true;led_green=false;led_red=false;}
			else if (led_green){ led_green = false;}
			else {led_green = true;}
		}
		if (strcmp((char*)data,(char*)comp_all) == 0){
			printf("Switching all LEDs \n\r");
			if (led_all){led_all=false;led_green=false;led_red=false;}
			else {led_all=true;led_green=false;led_red=false;}
		}
		if (strcmp((char*)data,(char*)buzz_sw) == 0){
			printf("Switching buzzer\n\r");
			if (buzzer){buzzer = false;} else {buzzer=true;}		
		}
		if (strcmp((char*)data,(char*)buzz_inc) == 0){
			printf("Increasing buzzer frequency\n\r");
			buzz_long=true;buzz_incr=true;
		}
		if (strcmp((char*)data,(char*)buzz_dec) == 0){
			printf("Decreasing buzzer frequency\n\r");
			buzz_long=true;buzz_incr=false;
		}
		if (strcmp((char*)data,(char*)tagip) == 0){
			ieee_addr_cpy_to(macAddress,8);
			printf("MAC Address is: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n\r",macAddress[0],macAddress[1],macAddress[2],macAddress[3],macAddress[4],macAddress[5],macAddress[6],macAddress[7]);
		}
		if(buzzer && buzz_long){
			counter_etimer++;			
			for (i=0;i<5;i++){
				if(buzz_incr){buzz_freq+=10;}
				else {buzz_freq-=10;}
				buzzer_start(buzz_freq);
				etimer_set(&timer_etimer,1 * CLOCK_SECOND);		
				PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
				buzzer_stop();										
			}
			buzz_long=false;
		} else if(buzzer){
			counter_etimer++;
			buzzer_start(buzz_freq);
			etimer_set(&timer_etimer,0.2 * CLOCK_SECOND);		
			PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
			buzzer_stop();
		}
	}
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
//LEDs Action thread
PROCESS_THREAD(process2, ev, data) {
	static struct etimer timer_etimer;
	PROCESS_BEGIN();

  while(1) {
	timer_set(&timer_timer, 1 * CLOCK_SECOND);
    etimer_set(&timer_etimer, CLOCK_SECOND);
    PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
    do_Actions();
	//PROCESS_YIELD();
  }

  PROCESS_END();
}
