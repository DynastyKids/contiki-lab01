/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Erbium (Er) REST Engine example.
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "rest-engine.h"
#include "contiki-lib.h"
// added
#include "buzzer.h"
#include "dev/leds.h"
#include "sys/etimer.h"
#include "shared_variables.h"
#include "mpu-9250-sensor.h"

#if PLATFORM_HAS_BUTTON
#include "dev/button-sensor.h"
#endif

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]", (lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3], (lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif

/*
 * Resources to be activated need to be imported through the extern keyword.
 * The build system automatically compiles the resources in the corresponding sub-directory.
 */
extern resource_t
  res_hello,
  res_mirror,
  res_chunks,
  res_separate,
  res_push,
  res_event,
  res_sub,
  res_gyrox,
  res_gyroy,
  res_gyroz,
  res_rssi,
  res_buzzer,
  res_b1_sep_b2;
#if PLATFORM_HAS_LEDS
extern resource_t res_toggle;
extern resource_t res_red;
extern resource_t res_green;
#endif
#if PLATFORM_HAS_LIGHT
#include "dev/light-sensor.h"
extern resource_t res_light;
#endif
#if PLATFORM_HAS_BATTERY
#include "dev/battery-sensor.h"
extern resource_t res_battery;
#endif
#if PLATFORM_HAS_TEMPERATURE
#include "dev/temperature-sensor.h"
extern resource_t res_temperature;
#endif

int xreadings[10];
int yreadings[10];
int zreadings[10];
static int curr = 0;
static int buzstate = 0;

PROCESS(red, "red process");
PROCESS(green, "green process");
PROCESS(er_example_server, "Erbium Example Server");
PROCESS(gyro, "gyro process");
PROCESS(buzzer, "buzzer process");
AUTOSTART_PROCESSES(&er_example_server, &red, &green, &gyro, &buzzer);

PROCESS_THREAD(er_example_server, ev, data) {
  PROCESS_BEGIN();

  PROCESS_PAUSE();

  PRINTF("Starting Erbium Example Server\n");
  
    // added
    //SENSORS_ACTIVATE(mpu_9250_sensor);
    //mpu_9250_sensor.configure(SENSORS_ACTIVE, MPU_9250_SENSOR_TYPE_ALL);
    
#ifdef RF_CHANNEL
  PRINTF("RF channel: %u\n", RF_CHANNEL);
#endif
#ifdef IEEE802154_PANID
  PRINTF("PAN ID: 0x%04X\n", IEEE802154_PANID);
#endif

  PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
  PRINTF("LL header: %u\n", UIP_LLH_LEN);
  PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
  PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

  /* Initialize the REST engine. */
  rest_init_engine();

  /*
   * Bind the resources to their Uri-Path.
   * WARNING: Activating twice only means alternate path, not two instances!
   * All static variables are the same for each URI path.
   */
    rest_activate_resource(&res_hello, "test/hello");
    rest_activate_resource(&res_push, "test/push");
    rest_activate_resource(&res_gyrox, "sensor/mpu/gyro/x");
    rest_activate_resource(&res_gyroy, "sensor/mpu/gyro/y");
    rest_activate_resource(&res_gyroz, "sensor/mpu/gyro/z");
    rest_activate_resource(&res_buzzer, "buzzer/toggle");
    rest_activate_resource(&res_rssi, "rssi");
#if PLATFORM_HAS_LEDS
  rest_activate_resource(&res_toggle, "actuators/toggle");
  // we added this
  rest_activate_resource(&res_red, "led/red/toggle");
  rest_activate_resource(&res_green, "led/green/toggle");
#endif
#if PLATFORM_HAS_LIGHT
  rest_activate_resource(&res_light, "sensors/light"); 
  SENSORS_ACTIVATE(light_sensor);  
#endif
#if PLATFORM_HAS_TEMPERATURE
  rest_activate_resource(&res_temperature, "sensors/temperature");  
  SENSORS_ACTIVATE(temperature_sensor);  
#endif

  /* Define application-specific events here. */
  while(1) {
    PROCESS_WAIT_EVENT();
#if PLATFORM_HAS_BUTTON
    if(ev == sensors_event && data == &button_sensor) {
      PRINTF("*******BUTTON*******\n");

      /* Call the event_handler for this application-specific event. */
      res_event.trigger();

      /* Also call the separate response example handler. */
      res_separate.resume();
    }
#endif /* PLATFORM_HAS_BUTTON */
  }                             /* while (1) */

  PROCESS_END();
}

// buzzer
PROCESS_THREAD(buzzer, ev, data) {
  	static char cmd;
  	PROCESS_BEGIN();
	buzzer_stop();	 
	while (1) {
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_CONTINUE);
        if (buzstate == 0) {
            buzstate = 1;
            buzzer_start(1000);
        } else {
            buzstate = 0;
            buzzer_stop();
        }
	}
  	PROCESS_END();		//End of thread
}

// red light
PROCESS_THREAD(red, ev, data) {
  	PROCESS_BEGIN();	//Start of thread
	leds_off(LEDS_RED);	 //Turn LED off.
	static struct etimer et;
	static unsigned char running = 0;
	while (1) {
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER || 
            ev == PROCESS_EVENT_CONTINUE);
        if (ev == PROCESS_EVENT_CONTINUE && running == 0) {
            running = 1;
            etimer_set(&et, CLOCK_SECOND);
            leds_on(LEDS_RED);
        } else if (ev == PROCESS_EVENT_CONTINUE && running == 1) {
            running = 0;
            leds_off(LEDS_RED);
        } else if (ev == PROCESS_EVENT_TIMER && running == 1) {
            etimer_set(&et, CLOCK_SECOND);
            leds_toggle(LEDS_RED);
        } 
	}
  	PROCESS_END();		//End of thread
}

// green light
PROCESS_THREAD(green, ev, data) {
  	PROCESS_BEGIN();	//Start of thread
	leds_off(LEDS_GREEN);	 //Turn LED off.
	static struct etimer et;
	static unsigned char running = 0;
	while (1) {
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER || 
            ev == PROCESS_EVENT_CONTINUE);
        if (ev == PROCESS_EVENT_CONTINUE && running == 0) {
            running = 1;
            etimer_set(&et, CLOCK_SECOND);
            leds_on(LEDS_GREEN);
        } else if (ev == PROCESS_EVENT_CONTINUE && running == 1) {
            running = 0;
            leds_off(LEDS_GREEN);
        } else if (ev == PROCESS_EVENT_TIMER && running == 1) {
            etimer_set(&et, CLOCK_SECOND);
            leds_toggle(LEDS_GREEN);
        } 
	}
  	PROCESS_END();		//End of thread
}

// gyro
PROCESS_THREAD(gyro, ev, data) {
  	PROCESS_BEGIN();
  	static struct etimer et;
  	curr = 0;
  	mpu_9250_sensor.configure(SENSORS_ACTIVE, MPU_9250_SENSOR_TYPE_ALL);
	etimer_set(&et, CLOCK_SECOND/5);
	while (1) {
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
        if (curr >= 10) curr = 0;
        xreadings[curr] = mpu_9250_sensor.value(MPU_9250_SENSOR_TYPE_GYRO_X);
        yreadings[curr] = mpu_9250_sensor.value(MPU_9250_SENSOR_TYPE_GYRO_Y);
        zreadings[curr] = mpu_9250_sensor.value(MPU_9250_SENSOR_TYPE_GYRO_Z);
        curr++;
        etimer_set(&et, CLOCK_SECOND/5);
	}
  	PROCESS_END();		//End of thread
}


