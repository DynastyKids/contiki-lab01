
#include <string.h>
#include "rest-engine.h"
#include "lib/sensors.h"
#include "mpu-9250-sensor.h"
#include "contiki.h"
#include "shared_variables.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki-lib.h"
#include "contiki-net.h"

static void res_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

PARENT_RESOURCE(res_rssi,
                "title=\"Sub-resource demo\"",
                res_get_handler,
                NULL,
                NULL,
                NULL);

static void
res_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
    REST.set_header_content_type(response, REST.type.TEXT_PLAIN);

    sprintf((char *)buffer, "RSSI %d", (signed short)packetbuf_attr(PACKETBUF_ATTR_RSSI));
    
    REST.set_response_payload(response, buffer, strlen((char *)buffer));
}



